package com.supervielle.challenge.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.supervielle.challenge.service.RelationshipService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class) //required for spring 4, otherwise annotations won't work
@WebMvcTest(RelationshipController.class)
public class RelationshipControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private RelationshipService relationshipService;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void shouldGetRelationship() throws Exception {
        when(relationshipService.check(1,2)).thenReturn("TI@");

        ResultActions resultActions = this.mockMvc.perform(get("/relaciones/1/2/"))
                .andDo(print())
                .andExpect(content().string("TI@"))
                .andExpect(status().isOk());
    }
}