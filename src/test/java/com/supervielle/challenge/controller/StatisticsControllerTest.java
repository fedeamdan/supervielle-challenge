package com.supervielle.challenge.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.supervielle.challenge.model.Statistics;
import com.supervielle.challenge.service.StatisticsService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class) //required for spring 4, otherwise annotations won't work
@WebMvcTest(StatisticsController.class)
public class StatisticsControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private StatisticsService statisticsService;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void shouldGetStatistics() throws Exception {
        Statistics expected = new Statistics(2, 4, 50.0);
        when(statisticsService.get()).thenReturn(expected);

        ResultActions resultActions = this.mockMvc.perform(get("/estadisticas"))
                .andDo(print())
                .andExpect(status().isOk());

        MvcResult result = resultActions.andReturn();
        String contentAsString = result.getResponse().getContentAsString();

        Statistics response = objectMapper.readValue(contentAsString, Statistics.class);
        Assert.assertTrue(expected.equals(response));
    }
}