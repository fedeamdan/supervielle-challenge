package com.supervielle.challenge.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.supervielle.challenge.model.Person;
import com.supervielle.challenge.service.PersonService;
import com.supervielle.challenge.validation.IdentityDocumentationType;
import com.supervielle.challenge.validation.Sex;
import org.junit.Assert;
import org.junit.Test; //Be careful do not import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;

import java.util.Optional;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class) //required for spring 4, otherwise annotations won't work
@WebMvcTest(PersonController.class)
public class PersonControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PersonService personService;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void shouldGetPerson() throws Exception {
        Person expected = new Person(1, "siempreviva 123", IdentityDocumentationType.DNI, "35242223", "argentina", Sex.M, 20);
        when(personService.get(1)).thenReturn(Optional.of(expected));

        ResultActions resultActions = this.mockMvc.perform(get("/personas/1"))
                .andDo(print())
                .andExpect(status().isOk());

        MvcResult result = resultActions.andReturn();
        String contentAsString = result.getResponse().getContentAsString();

        Person response = objectMapper.readValue(contentAsString, Person.class);
        Assert.assertTrue(expected.equals(response));
    }

    @Test
    public void shouldFailGettingPerson() throws Exception {
        when(personService.get(1)).thenReturn(Optional.empty());

        this.mockMvc.perform(get("/personas/1"))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void shouldCreatePerson() throws Exception {
        Person expectedWithoutId = new Person(null, "siempreviva 123", IdentityDocumentationType.DNI, "35242223", "argentina", Sex.M, 20);

        Person expectedWithId = new Person(1, "siempreviva 123", IdentityDocumentationType.DNI, "35242223", "argentina", Sex.M, 20);
        when(personService.save(expectedWithoutId)).thenReturn(expectedWithId);
        String personJson = "{\"address\": \"siempreviva 123\", \"identityDocumentationType\" : \"DNI\",\n" +
                "\t\"identityDocumentation\" : \"35242223\",\n" +
                "\t\"country\" : \"argentina\",\n" +
                "\t\"sex\" : \"M\",\n" +
                "\t\"age\": 20}";
        ResultActions resultActions = this.mockMvc.perform(post("/personas/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(personJson)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isCreated());

        MvcResult result = resultActions.andReturn();
        String contentAsString = result.getResponse().getContentAsString();

        Person response = objectMapper.readValue(contentAsString, Person.class);
        Assert.assertTrue(expectedWithId.equals(response));
    }

    @Test
    public void shouldFailCreatingPerson() throws Exception {
        this.mockMvc.perform(post("/personas/")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"invalidkey\": \"value\"}")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(content().string(containsString("The transaction could not be performed. Invalid parameters: ")))
                .andExpect(content().string(containsString("address=can't be blank")))
                .andExpect(content().string(containsString("identityDocumentation=can't be null")))
                .andExpect(content().string(containsString("country=can't be blank")))
                .andExpect(content().string(containsString("sex=must be one of M|F")))
                .andExpect(content().string(containsString("identityDocumentationType=must be one of LE|DNI|CARNET_EXTRANJERIA|RUC|PASAPORTE|PART_NACIMIENTO|OTROS")))
                .andExpect(content().string(containsString("age=minimum age \"18\"")))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldUpdatePerson() throws Exception {
        Person oldPerson = new Person(1, "siempreviva 1", IdentityDocumentationType.DNI, "352423", "argentina", Sex.M, 19);
        Person updatedPerson = new Person(1, "siempreviva 123", IdentityDocumentationType.DNI, "35242223", "argentina", Sex.M, 20);
        when(personService.get(1)).thenReturn(Optional.of(oldPerson));
        when(personService.update(updatedPerson)).thenReturn(updatedPerson);
        String personJson = "{\"address\": \"siempreviva 123\", \"identityDocumentationType\" : \"DNI\",\n" +
                "\t\"identityDocumentation\" : \"35242223\",\n" +
                "\t\"country\" : \"argentina\",\n" +
                "\t\"sex\" : \"M\",\n" +
                "\t\"age\": 20}";
        ResultActions resultActions = this.mockMvc.perform(put("/personas/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(personJson)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().is2xxSuccessful());

        MvcResult result = resultActions.andReturn();
        String contentAsString = result.getResponse().getContentAsString();

        Person response = objectMapper.readValue(contentAsString, Person.class);
        Assert.assertTrue(updatedPerson.equals(response));
    }

    @Test
    public void shouldFailToUpdatePerson() throws Exception {
        when(personService.get(1)).thenReturn(Optional.empty());
        String personJson = "{\"address\": \"siempreviva 123\", \"identityDocumentationType\" : \"DNI\",\n" +
                "\t\"identityDocumentation\" : \"35242223\",\n" +
                "\t\"country\" : \"argentina\",\n" +
                "\t\"sex\" : \"M\",\n" +
                "\t\"age\": 20}";
        this.mockMvc.perform(put("/personas/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(personJson)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void shouldDeletePerson() throws Exception {
        Person expected = new Person(1, "siempreviva 123", IdentityDocumentationType.DNI, "35242223", "argentina", Sex.M, 20);
        when(personService.get(1)).thenReturn(Optional.of(expected));
        doNothing().when(personService).deleteById(1);
        this.mockMvc.perform(delete("/personas/1"))
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    public void shouldFailDeletingPerson() throws Exception {
        when(personService.get(1)).thenReturn(Optional.empty());
        doNothing().when(personService).deleteById(1);
        this.mockMvc.perform(delete("/personas/1"))
                .andExpect(status().isBadRequest());
    }
}