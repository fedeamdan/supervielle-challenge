package com.supervielle.challenge.service;

import com.supervielle.challenge.model.Statistics;
import com.supervielle.challenge.repository.CountBySexAndCountry;
import com.supervielle.challenge.repository.StatisticsRepository;
import com.supervielle.challenge.validation.Sex;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;


@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(properties = {"spring.cache.type=none"})
public class StatisticsServiceTest {
    @MockBean
    StatisticsRepository statisticsRepository;

    @Autowired
    StatisticsService statisticsService;

    @Test
    public void getStatisticsSuccessfully() {
        Statistics expected = new Statistics(2, 4, 33.33);
        List<CountBySexAndCountry> list = Arrays.asList(new CountBySexAndCountry(Sex.M, expected.getCantidad_hombres(), "argentina"), new CountBySexAndCountry(Sex.F, expected.getCantidad_mujeres(), "peru"));
        when(statisticsRepository.query()).thenReturn(list);
        Statistics actual = statisticsService.get();
        assertEquals(expected, actual);
    }
}