package com.supervielle.challenge.service;

import com.supervielle.challenge.model.Person;
import com.supervielle.challenge.repository.PersonRepository;
import com.supervielle.challenge.validation.IdentityDocumentationType;
import com.supervielle.challenge.validation.Sex;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityNotFoundException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(properties = {"spring.cache.type=none"})
public class PersonServiceTest {
    @MockBean
    PersonRepository personRepository;

    @MockBean
    RelationshipService relationshipService;

    @Autowired
    PersonService personService;

    @Test
    public void getPersonSuccessfully() {
        Person expectedWithId = new Person(1, "siempreviva 123", IdentityDocumentationType.DNI, "35242223", "argentina", Sex.M, 20);
        Optional<Person> expected = Optional.of(expectedWithId);
        when(personRepository.findById(1)).thenReturn(expected);
        Optional<Person> actual = personService.get(1);
        verify(personRepository, times(1)).findById(1);
        assertTrue(expected.get().equals(actual.get()));
    }

    @Test
    public void sucursalNotFound() {
        when(personRepository.findById(1)).thenReturn(Optional.empty());
        Optional<Person> actual = personService.get(1);
        verify(personRepository, times(1)).findById(1);
        assertFalse(actual.isPresent());
    }

    @Test
    public void relateSuccessfully() {
        Person p1 = new Person(1, "siempreviva 123", IdentityDocumentationType.DNI, "35242223", "argentina", Sex.M, 20);
        Person p2 = new Person(2, "siempreviva 123", IdentityDocumentationType.DNI, "35242224", "argentina", Sex.M, 20);

        when(personRepository.findAllById(Arrays.asList(1,2))).thenReturn(Arrays.asList(p1,p2));
        doNothing().when(relationshipService).relate(1,2);
        personService.relate(1,2);
        verify(relationshipService, times(1)).relate(1, 2);
    }

    @Test (expected = EntityNotFoundException.class)
    public void relateFailed() {
        Person p1 = new Person(1, "siempreviva 123", IdentityDocumentationType.DNI, "35242223", "argentina", Sex.M, 20);
        when(personRepository.findAllById(Arrays.asList(1,2))).thenReturn(Arrays.asList(p1));
        personService.relate(1,2);
    }

}
