package com.supervielle.challenge.service;

import com.supervielle.challenge.repository.RelationshipRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;


@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(properties = {"spring.cache.type=none"})
public class RelationshipServiceTest {
    @MockBean
    RelationshipRepository relationshipRepository;

    @Autowired
    RelationshipService relationshipService;

    @Test
    public void shouldRelateSuccessfully() {
        doNothing().when(relationshipRepository).relate(1,2);
        relationshipService.relate(1,2);
    }

    @Test
    public void shouldCheckSuccessfully() {
        when(relationshipRepository.check(1,2)).thenReturn(new String[]{"TI@"});
        assertEquals("TI@", relationshipService.check(1,2));
    }

    @Test
    public void shouldDeleteSuccessfully() {
        doNothing().when(relationshipRepository).delete(1);
        relationshipService.delete(1);
    }
}