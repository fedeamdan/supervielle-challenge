package com.supervielle.challenge.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class StatisticsRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public List<CountBySexAndCountry> query() {
        return jdbcTemplate.query("select country, sex, count(*) AS count from person group by sex, country", new CountBySexAndCountryRowMapper());
    }
}
