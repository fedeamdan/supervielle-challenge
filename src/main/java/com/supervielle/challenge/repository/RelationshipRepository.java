package com.supervielle.challenge.repository;

import com.supervielle.challenge.model.GraphPerson;
import org.springframework.data.neo4j.annotation.Query;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RelationshipRepository extends Neo4jRepository<GraphPerson, Integer> {
    @Query("MERGE (p1:Person{pid:$idPadre, name:$idPadre}) MERGE (p2:Person{pid:$idHijo, name:$idHijo}) CREATE (p1)-[:ESPADREDE]->(p2)")
    public void relate(Integer idPadre, Integer idHijo);

    @Query("WITH [$id1,$id2] as pids optional match (hijo1)<--(padre1)<--(abuelo)-->(padre2) where hijo1.pid in pids and padre2.pid in pids\n" +
            "optional match (hijo2)<--(padre)-->(hijo3) where hijo2.pid in pids and hijo3.pid in pids\n" +
            "optional match (hijo4)<--(padre)<--(abuelo)-->(padre)-->(hijo5) where hijo4.pid in pids and hijo5.pid in pids\n" +
            "return case \n" +
            "when hijo1 is not null and padre2 is not null then 'TI@' \n" +
            "WHEN hijo2 is not null and hijo3 is not null then 'HERMAN@'\n" +
            "WHEN hijo4 is not null and hijo5 is not null then 'PRIM@'\n" +
            "ELSE 'No hay relacion' END")
    public String[] check(Integer id1, Integer id2);

    @Query("match (p:Person {pid: $id}) detach delete p")
    void delete(Integer id);
}
