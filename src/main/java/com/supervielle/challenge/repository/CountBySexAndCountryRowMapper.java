package com.supervielle.challenge.repository;

import com.supervielle.challenge.validation.Sex;
import org.springframework.jdbc.core.RowMapper;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CountBySexAndCountryRowMapper implements RowMapper<CountBySexAndCountry> {

    @Override
    public CountBySexAndCountry mapRow(ResultSet rs, int rowNum) throws SQLException {
        CountBySexAndCountry countBySexAndCountry = new CountBySexAndCountry();
        countBySexAndCountry.setSex(Sex.values()[rs.getInt("sex")]);
        countBySexAndCountry.setCount(rs.getInt("count"));
        countBySexAndCountry.setCountry(rs.getString("country"));
        return countBySexAndCountry;
    }
}