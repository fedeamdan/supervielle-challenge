package com.supervielle.challenge.repository;

import com.supervielle.challenge.validation.Sex;

public class CountBySexAndCountry {
    private Sex sex;
    private Integer count;
    private String country;

    public CountBySexAndCountry() {}

    public CountBySexAndCountry(Sex sex, Integer count, String country) {
        this.sex     = sex;
        this.count   = count;
        this.setCountry(country);
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
