package com.supervielle.challenge.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Retention(RUNTIME)
@Documented
@Constraint(validatedBy = AgeValidator.class)
public @interface AgeCheck {
    String minAge();
    String message() default "minimum age \"{minAge}\"";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}