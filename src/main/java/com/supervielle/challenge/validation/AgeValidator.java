package com.supervielle.challenge.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class AgeValidator implements ConstraintValidator<AgeCheck, Integer> {
    private Integer minAge;

    @Override
    public void initialize(AgeCheck annotation) {
        minAge = Integer.parseInt(annotation.minAge());
        if (minAge < 0 || minAge > 100) {
            throw new IllegalArgumentException("minAge parameter should be between 0 and 100");
        }
    }

    @Override
    public boolean isValid(Integer value, ConstraintValidatorContext context) {
        if (value == null) {
            return false;
        }
        return value >= minAge;
    }
}
