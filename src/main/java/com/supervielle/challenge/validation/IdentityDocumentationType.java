package com.supervielle.challenge.validation;

public enum IdentityDocumentationType {
    LE,
    DNI,
    CARNET_EXTRANJERIA,
    RUC,
    PASAPORTE,
    PART_NACIMIENTO,
    OTROS
}
