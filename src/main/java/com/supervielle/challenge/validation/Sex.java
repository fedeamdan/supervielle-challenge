package com.supervielle.challenge.validation;

public enum Sex {
    M,
    F
}
