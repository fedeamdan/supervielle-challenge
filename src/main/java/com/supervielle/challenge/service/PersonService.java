package com.supervielle.challenge.service;

import com.supervielle.challenge.model.Person;
import com.supervielle.challenge.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
public class PersonService {
    @Autowired
    PersonRepository personRepository;

    @Autowired
    RelationshipService relationshipService;

    @Autowired
    StatisticsService statisticsService;

    @Cacheable(cacheNames = "person", key = "#id")
    public Optional<Person> get(Integer id) {
        System.out.println("Getting person with id " + id + " from database");
        return personRepository.findById(id);
    }

    public Person save(Person p) {
        Person newPerson = personRepository.save(p);
        statisticsService.clearCache();
        return newPerson;
    }

    @CacheEvict(cacheNames = "person", key = "#id")
    public Person update(Person p) {
        Person newPerson = personRepository.save(p);
        statisticsService.clearCache();
        return newPerson;
    }

    @CacheEvict(cacheNames = "person", key = "#id")
    public void deleteById(Integer id) {
        statisticsService.clearCache();
        relationshipService.delete(id);
        personRepository.deleteById(id);
    }

    public void relate(Integer id1, Integer id2) {
        List<Person> personList = (List<Person>) personRepository.findAllById(Arrays.asList(id1, id2));
        if (personList.size() != 2) {
            String missing = "";
            missing += !personList.contains(id1) ? id1 + " " : "";
            missing += !personList.contains(id2) ? id2 : "";
            throw new EntityNotFoundException("No se pueden relacionar. Falta en la base de datos los siguientes ids: " + missing);
        }
        relationshipService.relate(id1, id2);
    }
}
