package com.supervielle.challenge.service;

import com.supervielle.challenge.model.Statistics;
import com.supervielle.challenge.repository.CountBySexAndCountry;
import com.supervielle.challenge.repository.StatisticsRepository;
import com.supervielle.challenge.validation.Sex;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.stereotype.Service;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.List;

@Service
@EnableCaching
public class StatisticsService {

    @Autowired
    StatisticsRepository statisticsRepository;

    @Cacheable(cacheNames = "statistics")
    public Statistics get() {
        Statistics statistics = new Statistics();
        List<CountBySexAndCountry> list = statisticsRepository.query();
        statistics.setCantidad_hombres(getSexCount(Sex.M, list));
        statistics.setCantidad_mujeres(getSexCount(Sex.F, list));
        statistics.setPorcentaje_argentinos(getPercentageOfArgentinians(list));
        return statistics;
    }

    private int getSexCount(Sex s, List<CountBySexAndCountry> list) {
        return list.stream()
                .filter(c -> c.getSex().equals(s))
                .map(item -> item.getCount())
                .reduce(0, (sum, item)-> sum + item);
    }

    private Double getPercentageOfArgentinians(List<CountBySexAndCountry> list) {
        int arg = list.stream()
                .filter(c -> "ARGENTINA".equals(c.getCountry().toUpperCase()))
                .map(item -> item.getCount())
                .reduce(0, (sum, item)-> sum + item);
        if (arg == 0) {
            return 0.0;
        }
        int total = list.stream()
                .map(item -> item.getCount())
                .reduce(0, (sum, item)-> sum + item);

        DecimalFormat df = new DecimalFormat("#.##");
        df.setRoundingMode(RoundingMode.FLOOR);
        return new Double(df.format(arg * 100.0 / total));
    }

    @CacheEvict(cacheNames = "statistics")
    public void clearCache() {
        System.out.println("Clearing statistics cache");
    }
}
