package com.supervielle.challenge.service;

import com.supervielle.challenge.repository.RelationshipRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RelationshipService {

    @Autowired
    RelationshipRepository relationRepository;

    public void relate(Integer id1, Integer id2) {
        relationRepository.relate(id1, id2);
    }

    public String check(Integer id1, Integer id2) {
        return relationRepository.check(id1, id2)[0];
    }

    public void delete(Integer id) {
        relationRepository.delete(id);
    }
}
