package com.supervielle.challenge.model;

import com.supervielle.challenge.validation.AgeCheck;
import com.supervielle.challenge.validation.EnumNamePattern;
import com.supervielle.challenge.validation.IdentityDocumentationType;
import com.supervielle.challenge.validation.Sex;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "person", uniqueConstraints = {@UniqueConstraint(columnNames = {"identity_documentation_type", "identity_documentation", "country", "sex"})})
public class Person implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Column(name = "address")
    @NotBlank(message = "can't be blank")
    private String address;

    @Column(name = "identity_documentation_type")
    @EnumNamePattern(regexp = "LE|DNI|CARNET_EXTRANJERIA|RUC|PASAPORTE|PART_NACIMIENTO|OTROS")
    @NotNull(message = "must be one of LE|DNI|CARNET_EXTRANJERIA|RUC|PASAPORTE|PART_NACIMIENTO|OTROS")
    private IdentityDocumentationType identityDocumentationType;

    @Column(name = "identity_documentation")
    @NotNull(message = "can't be null")
    private String identityDocumentation;

    @Column(name = "country")
    @NotBlank(message = "can't be blank")
    private String country;

    @Column(name = "sex")
    @EnumNamePattern(regexp = "M|F")
    @NotNull(message = "must be one of M|F")
    private Sex sex;

    @Column(name = "age")
    @AgeCheck(minAge = "18")
    private int age;


    public Person(){ } //Default constructor, required by Jackson

    public Person (Integer id,
                   String address,
                   IdentityDocumentationType identityDocType,
                   String identityDocumentation,
                   String country,
                   Sex sex,
                   int age) {
        this.setId(id);
        this.setAddress(address);
        this.setIdentityDocumentationType(identityDocType);
        this.setIdentityDocumentation(identityDocumentation);
        this.setCountry(country);
        this.setSex(sex);
        this.setAge(age);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public IdentityDocumentationType getIdentityDocumentationType() {
        return identityDocumentationType;
    }

    public void setIdentityDocumentationType(IdentityDocumentationType identityDocumentationType) {
        this.identityDocumentationType = identityDocumentationType;
    }

    public String getIdentityDocumentation() {
        return identityDocumentation;
    }

    public void setIdentityDocumentation(String identityDocumentation) {
        this.identityDocumentation = identityDocumentation;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }


    @Override
    public boolean equals(Object oPerson){
        Person p = (Person) oPerson;
        return getId() == p.getId()
            && getAddress().equals(p.getAddress())
            && getIdentityDocumentationType().equals(p.getIdentityDocumentationType())
            && getIdentityDocumentation().equals(p.getIdentityDocumentation())
            && getCountry().equals(p.getCountry())
            && getSex().equals(p.getSex())
            && getAge() == p.getAge();
    }
}