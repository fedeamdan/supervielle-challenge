package com.supervielle.challenge.model;

import java.io.Serializable;

public class Statistics implements Serializable {
    private int cantidad_hombres;
    private int cantidad_mujeres;
    private double porcentaje_argentinos;

    public Statistics() {
    }

    public Statistics(int males, int females, double percentageOfArg) {
        this.setCantidad_hombres(males);
        this.setCantidad_mujeres(females);
        this.setPorcentaje_argentinos(percentageOfArg);
    }

    public int getCantidad_hombres() {
        return cantidad_hombres;
    }

    public void setCantidad_hombres(int cantidad_hombres) {
        this.cantidad_hombres = cantidad_hombres;
    }

    public int getCantidad_mujeres() {
        return cantidad_mujeres;
    }

    public void setCantidad_mujeres(int cantidad_mujeres) {
        this.cantidad_mujeres = cantidad_mujeres;
    }

    public double getPorcentaje_argentinos() {
        return porcentaje_argentinos;
    }

    public void setPorcentaje_argentinos(double porcentaje_argentinos) {
        this.porcentaje_argentinos = porcentaje_argentinos;
    }

    @Override
    public boolean equals(Object oStatistics){
        Statistics s = (Statistics) oStatistics;
        return getCantidad_hombres() == s.getCantidad_hombres()
                && getCantidad_mujeres() == s.getCantidad_mujeres()
                && getPorcentaje_argentinos() == s.getPorcentaje_argentinos();
    }
}