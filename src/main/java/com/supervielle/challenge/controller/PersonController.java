package com.supervielle.challenge.controller;

import com.supervielle.challenge.model.Person;
import com.supervielle.challenge.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.validation.Valid;
import java.net.URI;
import java.util.Optional;

@RestController
@RequestMapping("/personas")
public class PersonController {

    @Autowired
    PersonService personService;

    @GetMapping("/{id}")
    public ResponseEntity<Person> getPerson(@PathVariable("id") Integer id) {
        Optional<Person> person = personService.get(id);
        if (!person.isPresent()) {
            System.out.println("Person with id " + id + " not found.");
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(person.get());
    }

    @PostMapping("/")
    public ResponseEntity<Person> createPerson(@RequestBody @Valid Person p)  {
        Person newPerson = personService.save(p);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        return new ResponseEntity<>(newPerson, headers, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity updatePerson(@PathVariable("id") Integer id, @RequestBody @Valid Person p)  {
        if (!personService.get(id).isPresent()) {
            System.out.println("Person with id " + id + " is not present.");
            return ResponseEntity.badRequest().body("Person with id " + id + " is not present.");
        }
        //Ensure the requested id is passed to the object to update.
        p.setId(id);

        Person newPerson = personService.update(p);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "application/json");
        return new ResponseEntity<>(newPerson, headers, HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable("id") Integer id) {
        if (!personService.get(id).isPresent()) {
            System.out.println("Person with id " + id + " is not present.");
            return ResponseEntity.badRequest().body("Person with id " + id + " is not present.");
        }
        personService.deleteById(id);
        return ResponseEntity.ok().build();
    }

    @PostMapping("/{id1}/padre/{id2}")
    public ResponseEntity relatePersons(@PathVariable("id1") Integer id1, @PathVariable("id2") Integer id2)  {
        personService.relate(id1, id2);
        return ResponseEntity.created(URI.create("/personas/" + id1 + "/padre/" + id2)).build();
    }
}
