package com.supervielle.challenge.controller;

import com.supervielle.challenge.service.RelationshipService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RelationshipController {

    @Autowired
    RelationshipService relationshipService;

    @GetMapping("/relaciones/{id1}/{id2}")
    public ResponseEntity checkRelationship(@PathVariable("id1") Integer id1, @PathVariable("id2") Integer id2) {
        return ResponseEntity.ok(relationshipService.check(id1, id2));
    }
}
